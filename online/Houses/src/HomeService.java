import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;

public interface HomeService {
    Double getRent();

    default LinkedList<Home> sortByRent(HashMap<Home, String> home) {
        LinkedList<Home> homes = new LinkedList<>(home.keySet());

        homes.sort(new Comparator<Home>() {
            public int compare(Home h1, Home h2) {
                return h1.rent.compareTo(h2.rent);
            }
        });
        return homes;
    }

    static LinkedList<Home> findByInfostructure(HashMap<Home, String> home, double maxRent, String location, double metresToLocation) {

        LinkedList<Home> homes = new LinkedList<>();
        for (Home h : home.keySet()) {
            for (HashMap.Entry<String, Double> entry : h.infoStructure.entrySet()){
                String loc = entry.getKey();
                Double distToLoc = entry.getValue();
                if (h.rent <= maxRent && loc.equals(location) && distToLoc <= metresToLocation)
                    homes.add(h);
            }
        }
        return homes;
    }
}
