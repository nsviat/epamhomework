import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;

abstract class Home implements HomeService {
    private String city;
    private String address;
    protected Double rent;
    private double size[] = new double[2];

    HashMap<String, Double> infoStructure = new HashMap<>();

    Home(String city, double rent) {
        this.city = city;
        this.rent = rent;
    }

    public Double getRent() {
        return rent;
    }
}


//for (Double d : h.infoStructure.in) {
//        if (d > metresToLocation)
//        homes.remove(h);
//        }