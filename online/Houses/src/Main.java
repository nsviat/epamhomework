import java.util.HashMap;
//шаблонний метод
public class Main {
    public static void main(String[] args) {
        HashMap<Home, String> homes = new HashMap<>();

        Multistorey m1 = new Multistorey("Lviv", 10000, 10, 160);
        m1.infoStructure.put("Kindergarten", 100.0);
        m1.infoStructure.put("Cinema", 78.0);
        m1.infoStructure.put("School", 1043.0);
        m1.infoStructure.put("Restaurant", 38.0);

        Multistorey m2 = new Multistorey("Kyiv", 14000, 5, 75);
        m2.infoStructure.put("School", 80.0);
        m2.infoStructure.put("Supermarket", 290.0);
        PrivateHouse ph1 = new PrivateHouse("Lviv", 14000, false);
        ph1.infoStructure.put("Bus stop", 456.0);
        PrivateHouse ph2 = new PrivateHouse("Odessa", 21000, true);
        ph1.infoStructure.put("Bus stop", 374.0);
        ph1.infoStructure.put("Cafe", 87.0);

        homes.put(m1, "Multistorey");
        homes.put(m2, "Multistorey");
        homes.put(ph1, "Private house");
        homes.put(ph2, "Private house");

        for (Home h : HomeService.findByInfostructure(homes, 16000, "Schoool", 100)) {
            System.out.println(h.getRent());
        }
    }
}
