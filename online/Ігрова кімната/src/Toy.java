import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

class Toy implements IToy {
    private String name;
    private double price;
    private String size;

    Toy(String name, String size, double price) {
        this.name = name;
        this.price = price;
    }

    protected String getName() {
        return name;
    }

    double getPrice() {
        return price;
    }

}
