import javax.swing.plaf.synth.SynthOptionPaneUI;
import java.util.*;

public  class Room implements RoomService{
    private String name;
    private double money;
    double moneyToLeft = money;  //for algorithm

    LinkedList<Toy> toys = new LinkedList<>(); //contain sall toys

    //all types of toys
    static Toy s_kub = new Toy("Cub", "s", 5);
    static Toy m_kub = new Toy("Cub", "m", 10);
    static Toy b_kub = new Toy("Cub", "b", 15);

    static Toy s_ball = new Toy("Ball", "s", 10);
    static Toy m_ball = new Toy("Ball", "m", 15);
    static Toy b_ball = new Toy("Ball", "b", 20);

    static Toy s_car = new Toy("Car", "s", 25);
    static Toy m_car = new Toy("Car", "m", 35);
    static Toy b_car = new Toy("Car", "b", 45);

    //num of toys
    int n_s_kub = 0, n_m_kub = 0, n_b_kub = 0;
    int n_s_ball = 0, n_m_ball = 0, n_b_ball = 0;
    int n_s_car = 0, n_m_car = 0, n_b_car = 0;

    Room(String name, double money){
        this.name = name;
        this.money = money;
        fillRoom(money);
    }

    public void fillRoom(double money){

        //deque with prices
        ArrayDeque<Double> prices = new ArrayDeque<>();

        //array with room prices
        double[] array = {s_kub.getPrice(), m_kub.getPrice(), b_kub.getPrice(), s_ball.getPrice()
                , m_ball.getPrice(), b_ball.getPrice(), s_car.getPrice(), m_car.getPrice(), b_car.getPrice()};
        //sorting of our array
        Arrays.sort(array);
        //filling in double List
        for (Double db:
             array) {
            prices.add(db);
        }

        //algorithm of filling room
        do {
            //sum of all prices which is in queue
            double sum = 0;
            for (Double pr :
                    prices) {
                sum += pr;
            }

            int iterations = (int)money / (int)sum;     //num of exect adding of toys(changes )
            for (int i = 0; i < iterations; i++) {
                if (money  >= s_kub.getPrice()) {
                    money -= s_kub.getPrice();
                    n_s_kub++;
                    toys.add(s_kub);
                    System.out.print("");
                }
                if (money >= m_kub.getPrice()) {
                    money -= m_kub.getPrice();
                    n_m_kub++;
                    toys.add(m_kub);
                    System.out.print("");
                }
                if (money >= b_kub.getPrice()) {
                    money -= b_kub.getPrice();
                    n_b_kub++;
                    toys.add(b_kub);
                    System.out.print("");
                }
                if (money  >= s_ball.getPrice()) {
                    money -= s_ball.getPrice();
                    n_s_ball++;
                    toys.add(s_ball);
                    System.out.println("");
                }
                if (money >= m_ball.getPrice()) {
                    money -= m_ball.getPrice();
                    n_m_ball++;
                    toys.add(m_ball);
                    System.out.println("");
                }
                if (money >= b_ball.getPrice()) {
                    money -= b_ball.getPrice();
                    n_b_ball++;
                    toys.add(b_ball);
                    System.out.println("");
                }
                if (money  >= s_car.getPrice()) {
                    money -= s_car.getPrice();
                    n_s_car++;
                    toys.add(s_car);
                }
                if (money >= m_car.getPrice()) {
                    money -= m_car.getPrice();
                    n_m_car++;
                    toys.add(m_car);
                }
                if (money >= b_car.getPrice()) {
                    money -= b_car.getPrice();
                    n_b_car++;
                    toys.add(b_car);
                }
            }
        } while (money - prices.getLast() >= 0);
    }

}