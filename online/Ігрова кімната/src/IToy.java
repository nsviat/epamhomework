import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

interface IToy {

    //sort by name
    static void sortByName(LinkedList<Toy> toys) {
        Collections.sort(toys, new Comparator<Toy>() {
            public int compare(Toy t1, Toy t2) {
                return t1.getName().compareTo(t2.getName());
            }
        });
        dispName(toys);
    }

    //sorting by price
    static void sortByPrice(LinkedList<Toy> toys) {
        double arr[] = new double[toys.size()];

        short i = 0;  //iterator

        for (Toy t :     //make array with prices
                toys) {
            arr[i] = t.getPrice();
            i++;
        }
        i = 0;

        Arrays.sort(arr);

        LinkedList<Toy> orderedToysByPrice = new LinkedList<>();  //List for sorted toys

        //convert array in list
        for (Toy toy :
                toys) {
            for (double v : arr) {
                if (toy.getPrice() == v)
                    orderedToysByPrice.add(toy);
            }
            i++;
        }

        dispName(orderedToysByPrice);
    }

    static void dispName(LinkedList<Toy> list) {
        int i = 1;
        for (Toy obj : list) {
            System.out.println(i + ") " + obj.getName());
            i++;
        }
    }
}
