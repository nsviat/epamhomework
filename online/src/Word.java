import com.sun.jdi.ObjectCollectedException;

import java.io.IOError;
import java.io.Serializable;

class Word implements Serializable {

    private Word(String word) {
    }

    static void pyramid(String word, byte conn) {
        char[] l = word.toCharArray();

        for (int i = 0; i < l.length; i++) {
            System.out.println();
            for (int j = 0; j < i + 1; j++)
                if (l[j] != ' ')
                    System.out.print(l[j]);
        }
        if (conn == (byte) 0)
            System.out.println();
    }

    static void reversePyramid(String word) {
        char[] l = word.toCharArray();

        for (int i = 0; i < l.length; i++) {
            System.out.println();
            for (int j = 0; j < l.length - i; j++)
                if (l[j] != ' ')
                    System.out.print(l[j]);
        }
        System.out.println();
    }

    static void fullPyramid(String word) {
        pyramid(word, (byte) 1);
        reversePyramid(word);
    }

    //one obj creation controll func
    private static int ins = 0;

    void oneObj() {
        if (ins == 0)
            ins++;
        else {
            throw new ObjectCollectedException("Can not create two objects of this class");
        }
    }
}