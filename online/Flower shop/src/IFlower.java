import java.util.LinkedList;

public interface IFlower {
    double getPrice();

    LinkedList<Flower> checkForFlower(String name, int num, String color);

    String toString();
}
