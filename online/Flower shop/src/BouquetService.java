public interface BouquetService {
    void addFlowers(String name, int num, String color, Flower f);

    void setPrice();

    double getPrice();
}
