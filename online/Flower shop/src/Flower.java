import java.util.LinkedList;

public final class Flower implements IFlower {
    private String name;
    private String color;
    private double price;
    private boolean isFlower;
    private static int numOfFlowers;
    private int num;

    //only if flowerpot
    private LinkedList<Flower> flowers;


    //constructor if just flowers
    Flower(String name, String color, double price, int num) {
        this.name = name;
        this.color = color;
        this.price = price;
        this.isFlower = true;
        this.num = num;
        numOfFlowers += this.num;
    }

    //return num of flowers
    int getNum() {
        return num;
    }

    //constructor if pair of flowers
    public Flower(String name, String color, double price, float length, LinkedList<Flower> flowers) {
        numOfFlowers += flowers.size();
        this.name = name;
        this.color = color;
        this.price = price;
        this.num = flowers.size();
        this.flowers = flowers;
        this.isFlower = false;
    }

    //return price
    public double getPrice() {
        return price;
    }


    //return List of appropriate flowers
    public LinkedList<Flower> checkForFlower(String name, int num, String color) {
        LinkedList<Flower> f = new LinkedList<>();
        try {
            if (this.name.toLowerCase().equals(name.toLowerCase())
                    && this.color.toLowerCase().equals(color.toLowerCase())
                    && this.num >= num) {
                f.add(new Flower(this.name, this.color, this.price, num));
                this.num -= num;
                return f;
            }
            else
                return null;
        }catch (NullPointerException n){
            System.out.println("Not enough " + name);
            return null;
        }
    }

    @Override
    public String toString() { return this.name; }
}
