import java.util.LinkedList;
import java.util.Objects;

class Bouquet implements BouquetService {
    LinkedList<Flower> flowers = new LinkedList<>();
    private double price;

    Bouquet(LinkedList<Flower> flowers) {
        this.flowers = flowers;
        setPrice();
    }

    public double getPrice() {
        return price;
    }

    //add flowers to bouquet
    public void addFlowers(String name, int num, String color, Flower f){
        try {
            this.flowers.addAll(f.checkForFlower(name, num, color));
        }
        catch (NullPointerException ignored){ }
        setPrice();
    }


    //set price
    public void setPrice() {
        price = 0;
        try {
            for (Flower f :
                    flowers) {
                price += f.getPrice() * f.getNum();
            }
        }
        catch (NullPointerException n){
            System.out.println("Nothing found");
        }
    }
}
