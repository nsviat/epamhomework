import javax.sound.midi.Soundbank;

public class Main {
    public static void main(String[] args) {
        Flower tulpan = new Flower("Tulpan", "blue", 40, 6);
        Flower mak = new Flower("Mak", "red", 40, 3);

        Bouquet b1 = new Bouquet(tulpan.checkForFlower("Tulpan", 4, "blue"));
        b1.addFlowers("Mak", 10, "red", mak);
        System.out.println(b1.getPrice());

        try {
            for (Flower f : b1.flowers)
                System.out.println(f.toString());
        }catch (NullPointerException n){
            System.out.println("No results");
        }
    }
}
