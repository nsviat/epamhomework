public class Main {
    //спостерігач
    public static void main(String[] args) {
        Simulator racetrack = new Simulator("Racetrack", new Exercise("run", 20));

        Simulator dumbbells = new Simulator("Dumbbells", new Exercise("big bar", 15));
        dumbbells.addExercise(new Exercise("small bar", 15));

        Simulator bar = new Simulator("Bar", new Exercise("tightening widely", 10));
        bar.addExercise(new Exercise("tightening narrowly", 10));
    }
}
