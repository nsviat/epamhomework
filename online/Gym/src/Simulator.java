import java.util.LinkedList;
import java.util.Objects;

public class Simulator implements ISimulation {
    private String name;
    private  String constructions;

    private LinkedList<Exercise> ex = new LinkedList<>();

    Simulator(String name, Object o) {
        this.ex.add((Exercise) o);
        this.name = name;
    }

    //if constructions needed
    public void setConstructions(String constructions) {
        this.constructions = constructions;
    }

    //get instructions
    public String getConstructions() {
        return constructions;
    }

    public void addExercise(Object o) {
        this.ex.add((Exercise) o);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
