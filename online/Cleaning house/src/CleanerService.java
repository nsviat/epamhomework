import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

public interface CleanerService {

    Double getPrice();

    static LinkedList<Cleaner> findByService(String name,
                                             String useCase , LinkedList<Cleaner> cleaners) {
        LinkedList<Cleaner> c = new LinkedList<>();

        for (Cleaner obj : cleaners){
            if (obj.useCases.equals(useCase) && obj.name.equals(name))
                c.add(obj);
        }

        c.sort(new Comparator<Cleaner>() {
            @Override
            public int compare(Cleaner o1, Cleaner o2) {
                return o1.getPrice().compareTo(o2.getPrice());
            }
        });

        return c;
    }
}
