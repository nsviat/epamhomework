import java.util.LinkedList;

public class Main {
    public static void main(String[] args) throws Exception {
        LinkedList<Jewelry> jewelries = new LinkedList<Jewelry>();

        Jewelry j = new Jewelry("12X12", 12.3, 1000, "SOLID");
        jewelries.add(j);
        Jewelry j1 = new Jewelry("10X12", 11.3, 2000, "SOLID");
        jewelries.add(j1);
        Jewelry j2 = new Jewelry("11X11", 14.7, 1240, "TRANSPARENT");
        jewelries.add(j2);
        Jewelry j3 = new Jewelry("10X10", 11.2, 4000, "SOLID");
        jewelries.add(j3);
        Jewelry j4 = new Jewelry("15X13", 16.9, 1300, "SEMITRANSPARENT");
        jewelries.add(j4);
        Jewelry j5 = new Jewelry("11X10", 14.1, 1346, "SOLID");
        jewelries.add(j5);

        Jewelry.dispJewelries(Necklace.findByClarity(jewelries, "SOLID"));
        System.out.println("----------------");
        Necklace n1 = new Necklace("Name1", jewelries);
        n1.dispAllStones();
    }
}
