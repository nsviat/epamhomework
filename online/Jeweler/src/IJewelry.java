import java.util.InputMismatchException;
import java.util.LinkedList;

public interface IJewelry {
    Jewelry.Clarity getClarity();

    double getMass();

    Double getPrice();

    void setSize(String size);

    //determine price of jewelry
    static double determinePrice(double mass, double pricePerGramm){
        double price = mass*pricePerGramm;
        return price;
    }

    //determine value of jewelry
    static Jewelry.Value determineValue(double mass, double pricePerGramm){
        if (determinePrice(mass, pricePerGramm) < 2000)
            return Jewelry.Value.semiprecious;
        else return Jewelry.Value.precious;
    }

    //determine clarity of jewelry
    static Jewelry.Clarity determineClarity(String clarity) throws Exception {
        if (clarity == "TRANSPARENT")
            return Jewelry.Clarity.TRANSPARENT;
        else if (clarity == "SEMITRANSPARENT")
            return Jewelry.Clarity.SEMITRANSPARENT;
        else if (clarity == "SOLID")
            return Jewelry.Clarity.SOLID;
        else
            throw new Exception("Incorrect type of clarity");
    }

    //check for correct size entering
    static void checkSize(String size){
        String[] s = size.split("X");

        for (String value : s) {
            char[] ch = value.toCharArray();
            for (char c : ch) {
                if (Integer.parseInt(Character.toString(c)) < 0 || Integer.parseInt(Character.toString(c)) > 9)
                    throw new InputMismatchException("Wrong input");
            }
        }
    }


    //disp claritu of jewelries
    static void dispJewelries(LinkedList<Jewelry> jewelries){
        for (Jewelry j:
                jewelries){
            System.out.println(j.clarity);
        }
    }
}
