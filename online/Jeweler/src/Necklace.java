import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

public class Necklace implements INecklace{

    private String name;
    private double price;
    private int numOfStones;
    private double mass;
    private LinkedList<Jewelry> jewelries = new LinkedList<>();

    public double getPrice() { return price; }

    public Necklace(String name, LinkedList<Jewelry> jewelries){
        this.name = name;
        fillNecklace(jewelries);
        this.numOfStones = jewelries.size()+1;
    }

    //fill and determine price and number of stones in necklace
    public void fillNecklace(LinkedList<Jewelry> jewelries){
        this.jewelries.addAll(jewelries);

        for (Jewelry j:
             jewelries) {
            this.price += j.getPrice();
            this.mass += j.getMass();
        }
    }

    public void dispAllStones(){
        IJewelry.dispJewelries(this.jewelries);
    }
}
