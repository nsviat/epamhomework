import java.util.InputMismatchException;
import java.util.LinkedList;

public class Jewelry implements IJewelry {
    private String[] size = new String[2];
    private double mass;
    private double pricePerGramm;
    private Double price;
    private Value value;
    Clarity clarity;


    //types 0f clarity
    protected enum Clarity{
        TRANSPARENT("TRANSPARENT"),
        SEMITRANSPARENT("SEMITRANSPARENT"),
        SOLID("SOLID");

        private String clar;

        Clarity(String clar){
            this.clar = clar;
        }
    }

    //precious/semiprecious
    enum Value{
        precious,
        semiprecious
    }

    public Clarity getClarity() { return clarity; }

    public double getMass() { return mass; }

    public Double getPrice() { return price; }

    //set weight, price per gramm and price of jewelry
    Jewelry(String size, double mass, double pricePerGramm, String clarity) throws Exception {
        this.mass = mass;
        this.pricePerGramm = pricePerGramm;
        this.price = IJewelry.determinePrice(mass, pricePerGramm);
        this.value = IJewelry.determineValue(mass, pricePerGramm);
        this.clarity = IJewelry.determineClarity(clarity);
        setSize(size);
    }

    //set size(weight, height)
    public void setSize(String size){
        IJewelry.checkSize(size);
        String[] s = size.split("X");
        this.size[0] = s[0];
        this.size[1] = s[1];
    }
}
