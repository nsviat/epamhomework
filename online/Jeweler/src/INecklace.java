import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

public interface INecklace {

    void fillNecklace(LinkedList<Jewelry> jewelries);

    void dispAllStones();

    //sort stones by price
    static LinkedList<Jewelry> sortStones(LinkedList<Jewelry> jewelries){
        Collections.sort(jewelries, new Comparator<Jewelry>() {
            public int compare(Jewelry j1, Jewelry j2) {
                return j1.getPrice().compareTo(j2.getPrice());
            }
        });

        return jewelries;
    }

    //find stonrs by clarity
    static LinkedList<Jewelry> findByClarity(LinkedList<Jewelry> jewelries, String clarity) throws Exception {
        if (clarity != "TRANSPARENT" && clarity != "SEMITRANSPARENT" && clarity != "SOLID")
            throw new Exception("Incorrect type of clarity");

        LinkedList<Jewelry> list = new LinkedList<>();

        for (Jewelry j:
                jewelries) {
            if (j.getClarity().toString() == clarity)
                list.add(j);
        }

        return list;
    }
}
