import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;

public class CheapPlane implements CompanyService {
    private static final String name = "CheapPlane";
    private int numOfPlanes;
    private int peopleCapacity;
    private double maxWeight;

    LinkedList<Plane> planes = new LinkedList<>();

    CheapPlane(LinkedList<Plane> planes) {
        this.planes = planes;
        this.numOfPlanes = planes.size();
        setPeopleCapacity();
        setPackagesCapacity();
    }

    public void setPeopleCapacity() {
        for (Plane pl : planes)
            peopleCapacity += pl.getPassangersNum();
    }

    public void setPackagesCapacity() {
        for (Plane pl : planes)
            maxWeight += pl.getMaxWeight();
    }

    public double getMaxWeight() { return maxWeight; }

    public Integer getPeopleCapacity() { return peopleCapacity; }

    LinkedList<Plane> sortByFlightDistance(){
        LinkedList<Plane> sortedPlanes = planes;

        sortedPlanes.sort(new Comparator<Plane>() {
            public int compare(Plane p1, Plane p2) {
                return p1.getFlightDistance().compareTo(p2.getFlightDistance());
            }
        });

        return sortedPlanes;
    }

    public LinkedList<Plane> findByFuel(double min, double max){
        LinkedList<Plane> foundPlanes = new LinkedList<>();

        for (Plane pl : planes){
            if (pl.getFuelCapacity() >= min && pl.getFuelCapacity() <= max)
                foundPlanes.add(pl);
        }
        return foundPlanes;
    }
}
