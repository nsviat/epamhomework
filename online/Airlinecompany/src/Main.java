import java.util.LinkedList;
//iterator
public class Main {

    public static void main(String[] args) {
        LinkedList<Plane> planes = new LinkedList<>();
        planes.add(new Plane("boeing322", 78, 3834.23, 9000, 723, 820));
        planes.add(new Plane("boeing2", 50, 3300.16, 6200, 694, 732));
        planes.add(new Plane("boeing3", 90, 3100, 10200, 637, 764));
        planes.add(new Plane("boeing4", 49, 5000.85, 6100, 816, 820));
        planes.add(new Plane("boeing5", 100, 7844.62, 11200, 938, 720));
        planes.add(new Plane("boeing6", 20, 1000.23, 3200, 500, 773));
        planes.add(new Plane("boeing7", 45, 1924.51, 5700, 584, 783));
        planes.add(new Plane("boeing8", 49, 3834.34, 6200, 719, 793));
        planes.add(new Plane("boeing9", 93, 2941.61, 10500, 749, 915));
        planes.add(new Plane("boeing10", 90, 8293, 10200, 1000, 862));

        CheapPlane company = new CheapPlane(planes);
        CompanyService.dispPlanes(company.sortByFlightDistance());
        System.out.println("--------");
        CompanyService.dispPlanes(company.findByFuel(600,700));
    }
}
