public interface PlaneService {
    int getPassangersNum();

    double getMaxWeight();

    Double getFlightDistance();

    double getFuelCapacity();
}
