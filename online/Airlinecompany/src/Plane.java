import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.LinkedList;

class Plane implements PlaneService{
    private String model;
    private int passangersNum;
    private Double flightDistance;
    private double fuelCapacity;
    private double speed;
    private double maxWeight;

    Plane(String model, int passangersNum, double flightDistance, double maxWeight, double fuelCapacity, double speed) {
        this.model = model;
        this.passangersNum = passangersNum;
        this.flightDistance = flightDistance;
        this.maxWeight = maxWeight;
        this.fuelCapacity = fuelCapacity;
        this.speed = speed;
    }

    public int getPassangersNum() { return passangersNum; }

    public double getMaxWeight() { return maxWeight; }

    public Double getFlightDistance() { return flightDistance; }

    public double getFuelCapacity() { return fuelCapacity; }

}
