import java.util.LinkedList;

public interface CompanyService {
    void setPeopleCapacity();

    void setPackagesCapacity();

    LinkedList<Plane> findByFuel(double min, double max);

    static void dispPlanes(LinkedList<Plane> planes){
        for (Plane pl : planes)
            System.out.println(pl.getFlightDistance());
    }
}
