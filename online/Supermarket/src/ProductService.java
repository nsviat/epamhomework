import java.util.LinkedList;

interface ProductService {
    //find in range
    static LinkedList<Product> find(double minPrice, double maxPrice, LinkedList<Product> list) {
        LinkedList<Product> ls = new LinkedList<>();
        for (Product pb:
                list) {
            if (minPrice <= pb.price && pb.price <= maxPrice) {
                ls.add(pb);
            }
        }
        return ls;
    }

    //find exact value
    static LinkedList<Product> find(double priceToFind, LinkedList<Product> list) {
        LinkedList<Product> ls = new LinkedList<>();
        for (Product pb:
                list) {
            if (pb.price == priceToFind) {
                ls.add(pb);
            }
        }
        return ls;
    }

    //found number of coincidences in finding in range
    static int foundNumber(double minPrice, double maxPrice, LinkedList<Product> list) {
        return ProductService.find(minPrice, maxPrice, list).size();
    }

    //found number of coincidences in finding exact price
    static int foundNumber(double priceToFind, LinkedList<Product> list) {
        return ProductService.find(priceToFind, list).size();
    }

    //display every name in product
    static void dispName(LinkedList<Product> list){
        int i = 1;
        for (Product obj: list) {
            System.out.println(i + ") " + Product.getName(obj));
            i++;
        }
    }

    //display every price in product
    static void dispPrice(LinkedList<Product> list){
        int i = 1;
        for (Product obj: list) {
            System.out.println(i + ") " + Product.getPrice(obj));
            i++;
        }
    }
}
