import java.util.LinkedList;

abstract class Product implements ProductService{

    protected String name;
    protected double price;

    Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    Product(){}

    protected static String getName(Product product){
        return product.name;
    }

    protected static double getPrice(Product product){
        return product.price;
    }
}

