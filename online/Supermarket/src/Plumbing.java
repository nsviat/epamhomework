import java.util.LinkedList;

class Plumbing extends Product {

    Plumbing(String name, double price){
        super(name, price);
    }

    //additional parametrs can be added here

    Plumbing(){ super(); }
}
