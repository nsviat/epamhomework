import Payment.IEPayment;
import Payment.PayPal;
import Payment.Payment;
import Post.DeliveringTroubles;
import Post.NewPost;
import Post.Post;
import Products.*;
import User.User;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class CashRegister {

    private static ArrayList<Product> products = new ArrayList<>();
    private static ArrayList<User> users = new ArrayList<>();
    private static ArrayList<Post> posts = new ArrayList<Post>();

    static User user;


    public static void main(String[] args) {
        controller();
    }


    static void controller() {
        adminPanel();
        Scanner sc = new Scanner(System.in);
        int choose = -1;
        while (choose != 0) {
            System.out.println("Here are our users: ");
            for (User u :
                    User.getAllUsers()) {
                System.out.println(u.getName());
            }
            System.out.print("Select user by name: ");
            user = User.getUser(sc.next());
            addToCart(user);
            payment(user);
            System.out.println("\n\n\n\n====================");
            System.out.print("If u want exit shop press 0, otherwise press anything else");
            users.remove(user);
            choose = sc.nextInt();
        }
    }

    static void addToCart(User user) {

        Scanner sc = new Scanner(System.in);
        System.out.println("\n\n==============================\nAdd products to your cart(for exit enter 0): \n");
        dispProducts();
        String forLoopEnd = "yes";
        while (!forLoopEnd.equals("no")) {
            int productIdex = sc.nextInt() - 1;
            user.getCart().addProduct(products.get(productIdex));
            products.remove(products.get(productIdex));
            System.out.println("Continue(yes/no)?");
            forLoopEnd = sc.next();
            dispProducts();
        }
    }

    static double delivery(User user) throws DeliveringTroubles {
        Scanner sc = new Scanner(System.in);

        double change = -1;

        dispPosts();
        System.out.println("From where and to which posy do u want deliver your order?");
        Post post = posts.get(sc.nextInt() - 1);
        Post endPost = posts.get(sc.nextInt() - 1);
        assert post != null;
        change = post.payForDeliver(user, endPost);
        if (change >= 0) {
            System.out.println("Thank you");
            return change;
        } else {
            System.out.println("No money");
            return change;
        }
    }

    static double payment(User user) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Select payment method(offline or online ): ");
        String paymentMethod = sc.next();

        double change = 0;

        if (paymentMethod.equals("offline")) {
            change = Payment.payOffline(user);
            users.remove(findUser(user.getName()));
            System.out.println("Thank you, tour change is: " + change);
        } else if (paymentMethod.equals("online")) {
            IEPayment payment = new PayPal();
            if (payment.setRequestToServer(user))
                if (payment.getResponse(user) >= 0)
                    if (payment.obtainRequest(user)) {
                        try {
                            System.out.println("Checking payment service... ");
                            try {
                                Thread.sleep(1500);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            System.out.println("Payment completed");
                            delivery(user);
                        } catch (DeliveringTroubles deliveringTroubles) {
                            deliveringTroubles.printStackTrace();
                        }
                        return change;
                    }
        }

        return change;
    }

    static Post selectPost(String name) {
        for (Post p :
                posts) {
            if (p.getName().toLowerCase().equals(name))
                return p;
        }
        return null;
    }

    static void adminPanel() {

        Product tree1 = null;
        Product flower1 = null;
        Product flower2 = null;
        Product flower3 = null;
        Product flower4 = null;
        Product ribbon1 = null;
        Product wrapper1 = null;

        users.add(new User("Sviat", "lazarenka48a", 10000));

        posts.add(new NewPost("відділення 13", "стрийська 23"));
        posts.add(new NewPost("відділення 5", "зелена 14"));
        posts.add(new NewPost("відділення 1", "чорновола 59"));

        tree1 = new Tree("Palm", 700);
        flower1 = new Flower("Tulpan", 25);
        flower2 = new Flower("Troyanda", 30);
        flower3 = new Flower("Romashka", 20);
        flower4 = new Flower("Chryzantema", 45);
        ribbon1 = new Ribbon("ribbon1", 10);
        wrapper1 = new Wrapper("wrapper1", 10);

        products.add(tree1);
        products.add(flower1);
        products.add(flower2);
        products.add(flower3);
        products.add(flower4);
        products.add(ribbon1);
        products.add(wrapper1);

        Scanner sc = new Scanner(System.in);
        int choose = -1;
        while (choose != 0) {
            System.out.println("For exit products adding admin mode enter 0.");
            System.out.println("For user adding enter 1");
            System.out.println("For flower adding enter 2");
            System.out.println("For tree adding enter 3");
            System.out.println("For ribbon adding enter 4");
            System.out.println("For wrapper adding enter 5");
            System.out.println("For wrapper adding enter 6");
            System.out.println("For bouquet adding enter 7");

            choose = sc.nextInt();

            switch (choose) {
                case 0:
                    break;
                case 1:
                    addUser();
                    break;
                case 2:
                    try {
                        createProduct("flower");
                    } catch (NoSuchObject noSuchObject) {
                        noSuchObject.printStackTrace();
                    }
                    break;
                case 3:
                    try {
                        createProduct("tree");
                    } catch (NoSuchObject noSuchObject) {
                        noSuchObject.printStackTrace();
                    }
                    break;
                case 4:
                    try {
                        createProduct("ribbon");
                    } catch (NoSuchObject noSuchObject) {
                        noSuchObject.printStackTrace();
                    }
                    break;
                case 5:
                    try {
                        createProduct("wrapper");
                    } catch (NoSuchObject noSuchObject) {
                        noSuchObject.printStackTrace();
                    }
                case 6:
                    try {
                        createProduct("vase");
                    } catch (NoSuchObject noSuchObject) {
                        noSuchObject.printStackTrace();
                    }
                case 7:
                    try {
                        createProduct("bouquet");
                    } catch (NoSuchObject noSuchObject) {
                        noSuchObject.printStackTrace();
                    }
                    default:
                        System.out.println("Incorrect input");
            }

        }
    }

    static void dispProducts() {
        int index = 1;
        System.out.println("\n\n=====================================================>");
        for (Product p :
                products) {
            System.out.println(index + "  --  " + p.toString());
            index++;
        }
        System.out.println("<======================================================");
    }

    static void dispPosts() {

        int index = 1;
        System.out.println("\n\n=====================================================>");
        System.out.println("Список відділень: ");
        for (Post p :
                posts) {
            System.out.println(index + "  --  " + p.getName());
            index++;
        }
        System.out.println("<======================================================");
    }

    static Bouquet createBouquet() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter name of bouquet: ");
        String name = sc.next();
        ArrayList<Flower> flowers = new ArrayList<>();
        int index = 1;
        int forExitLoop = -1;
        System.out.println("Choose flower.\nFor exit enter 0.\n=====================================================>");
        dispFlowers();
        while (forExitLoop != 0) {
            System.out.print("flower " + index + "  --  ");
            int indexFlower = sc.nextInt();
            flowers.add((Flower) products.get(indexFlower));
            products.remove(indexFlower);
            index++;
            System.out.print("If you finished add flowers press 0, otherwise press any number: ");
            forExitLoop = sc.nextInt();
        }
        Bouquet b = new Bouquet(name, flowers);
        products.add(b);
        return b;
    }

    static void dispFlowers() {
        System.out.println("\n\n=====================================================>");
        for (Product p :
                products) {
            if (p.getClass() == Flower.class)
                System.out.println(products.indexOf(p) + "  --  " + p.toString());
        }
        System.out.println("\n\n<======================================================");
    }

    static void dispUsers() {
        for (User u :
                users) {
            System.out.println(u.toString());
        }
    }

    static Product findProduct(String name) {
        for (Product p :
                products) {
            if (p.getName().toLowerCase().equals(name.toLowerCase())) {
                System.out.println(p.getName());
                return p;
            } else {
                throw new NoSuchElementException("Did not find product");
            }
        }
        return null;
    }

    static Flower findFlower(String name) {
        for (Product p :
                products) {
            if (p.getName().toLowerCase().equals(name.toLowerCase())) {
                System.out.println(p.getName());
                return (Flower) p;
            } else {
                throw new NoSuchElementException("Did not find product");
            }
        }
        return null;
    }

    static User findUser(String name) {
        for (User p :
                users) {
            if (p.getName().toLowerCase().equals(name.toLowerCase())) {
                System.out.println(p.getName());
                return p;
            } else {
                throw new NoSuchElementException("Did not find product");
            }
        }
        return null;
    }

    static void addUser() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter user name: ");
        String name = sc.next();
        System.out.print("Enter user adress: ");
        String adress = sc.next();
        System.out.print("Enter user balance: ");
        double balance = sc.nextDouble();

        users.add(new User(name, adress, balance));

        dispUsers();
    }

    static void createProduct(String productType) throws NoSuchObject {

        Scanner sc = new Scanner(System.in);

        String name;
        double price;
        String color;
        String description;

        switch (productType) {

            case "flower":
                System.out.print("Enter name: ");
                name = sc.next();
                System.out.print("Enter price: ");
                price = sc.nextDouble();
                System.out.print("Enter description: ");
                description = sc.next();
                System.out.print("Enter time to life: ");
                String timeToLife = sc.next();
                System.out.print("Enter water consuming: ");
                String waterComsuming = sc.next();
                System.out.print("Enter color: ");
                color = sc.next();
                products.add(new Flower(name, price, description, timeToLife, waterComsuming, color));
                dispProducts();
                break;
            case "cactus":
                System.out.print("Enter name: ");
                name = sc.next();
                System.out.print("Enter price: ");
                price = sc.nextDouble();
                System.out.print("Enter color: ");
                color = sc.next();
                products.add(new Cactus(name, price, color));
                dispProducts();
                break;
            case "tree":
                System.out.print("Enter name: ");
                name = sc.next();
                System.out.print("Enter price: ");
                price = sc.nextDouble();
                System.out.print("Enter description: ");
                description = sc.next();
                System.out.print("Enter time to life: ");
                timeToLife = sc.next();
                System.out.print("Enter water consuming: ");
                waterComsuming = sc.next();
                System.out.print("Is fruits enabled?: ");
                boolean fruitsEnabled = sc.nextBoolean();
                products.add(new Tree(name, price, description, timeToLife, waterComsuming, fruitsEnabled));
                dispProducts();
                break;
            case "ribbon":
                System.out.print("Enter name: ");
                name = sc.next();
                System.out.print("Enter price: ");
                price = sc.nextDouble();
                System.out.print("Enter length: ");
                double length = sc.nextDouble();
                System.out.print("Enter color: ");
                color = sc.next();
                products.add(ProductFactory.createProduct("ribbon", name, price));
                dispProducts();
                break;
            case "wrapper":
                System.out.print("Enter name: ");
                name = sc.next();
                System.out.print("Enter price: ");
                price = sc.nextDouble();
                System.out.print("Enter description: ");
                description = sc.next();
                System.out.print("Enter transparency: ");
                String transparency = sc.next();
                System.out.print("Enter picture: ");
                String picture = sc.next();
                products.add(ProductFactory.createProduct("wrapper", name, price));
                dispProducts();
                break;
            case "vase":
                System.out.print("Enter name: ");
                name = sc.next();
                System.out.print("Enter price: ");
                price = sc.nextDouble();
                System.out.println("Enter material: ");
                String material = sc.next();
                products.add(ProductFactory.createProduct("vase", name, price, material));
                dispProducts();
                break;
            default:
                throw new NoSuchElementException("There is no such element");
            case "bouquet":
                System.out.print("Enter name of bouquet: ");
                name = sc.next();
                ArrayList<Flower> flowers = new ArrayList<>();
                int index = 1;
                int forExitLoop = -1;
                System.out.println("Choose flower.\nFor exit enter 0.\n=====================================================>");
                dispFlowers();
                while (forExitLoop != 0) {
                    System.out.print("flower " + index + "  --  ");
                    int flowerInd = sc.nextInt();
                    flowers.add((Flower) products.get(flowerInd));
                    products.remove(flowerInd);
                    index++;
                    dispFlowers();
                    System.out.print("If you finished add flowers press 0, otherwise press any number: ");
                    forExitLoop = sc.nextInt();
                }
                products.add(new Bouquet(name, flowers));
        }
    }

}
