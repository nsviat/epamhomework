package Payment;

import User.User;

public interface IEPayment {
    boolean setRequestToServer(User user);

    boolean obtainRequest(User user);

    double  getResponse(User user);
}
