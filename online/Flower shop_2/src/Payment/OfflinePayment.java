package Payment;

import User.User;

public class OfflinePayment implements IPayment {
    @Override
    public double pay(User user) throws NotEnoghMoney {
        double balance = user.getBalance();
        if (user.getBalance() - user.getCart().getSumm() >= 0){
            user.setBalance(user.getBalance()-user.getCart().getSumm());
            user.getCart().removeAllProducts();
            return balance - user.getBalance();
        }
        else
            throw new NotEnoghMoney();
    }
}
