package Payment;

import User.User;

public class Payment {

    private static IPayment paymentOffline = new OfflinePayment();

    private static IEPayment paymentOnline;

    static void setOnlinePaymentMethod(IEPayment iePayment){
        paymentOnline = iePayment;
    }


    public static double payOffline(User user){
        try {
            return paymentOffline.pay(user);
        } catch (NotEnoghMoney notEnoghMoney) {
            notEnoghMoney.printStackTrace();
        }
        return 0;
    }

    public static double payOnline(User user){
        paymentOnline.setRequestToServer(user);
        paymentOnline.obtainRequest(user);
        return paymentOnline.getResponse(user);
    }
}
