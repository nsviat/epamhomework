package Payment;

import User.User;

public class PayPal implements IEPayment  {
    //some variables from Payment.PayPal API

    @Override
    public boolean setRequestToServer(User user) {
        return true;
    }

    @Override
    public boolean obtainRequest(User user) {
        return true;
    }

    @Override
    public double getResponse(User user) {
        double balance = Payment.payOffline(user);
        return balance;
    }
}
