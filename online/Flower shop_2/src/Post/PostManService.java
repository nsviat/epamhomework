package Post;

import Products.NoSuchObject;

public interface PostManService {

    boolean addPostman(Postman p1);

    Postman findPostman(Postman p1) throws NoSuchObject;
}
