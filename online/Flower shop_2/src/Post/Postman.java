package Post;

public class Postman {
    private String name;
    private double salary;
    private String workTimel;

    public Postman(String name, double salary, String workTimel) {
        this.name = name;
        this.salary = salary;
        this.workTimel = workTimel;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getWorkTimel() {
        return workTimel;
    }

    public void setWorkTimel(String workTimel) {
        this.workTimel = workTimel;
    }
}
