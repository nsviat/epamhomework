package Post;

import Payment.*;
import User.User;

import java.util.ArrayList;

public abstract class Post implements DeliverService, PostManService {
    private String name;
    private String location;

    private ArrayList<Postman> postmen = new ArrayList<>();

    Post(String name, String location) {
        this.name = name;
        this.location = location;
    }

    abstract boolean del(User user, Post post);

   public double payForDeliver(User user, Post post) throws DeliveringTroubles {
        if (del(user, post)) {
            return Payment.payOffline(user);
        }
        else
            throw new DeliveringTroubles();
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean addPostman(Postman p1) {
            if (findPostman(p1) == null) {
                postmen.add(p1);
                return true;
            }
            else
                return false;
    }

    @Override
    public Postman findPostman(Postman p1){
        boolean found = false;

        for (Postman p2:
             postmen) {
            if (p2.getName().equals(p1.getName())) {
                found = true;
                break;
            }
        }
        if (found)
            return p1;
        else
            return null;
    }
}
