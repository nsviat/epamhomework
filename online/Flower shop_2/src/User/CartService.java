package User;

import Products.Product;

import java.util.ArrayList;

public interface CartService {
    void addProduct(Product product);

    void addProducts(ArrayList<Product> product);

    boolean removeProduct(Product product);

    void removeAllProducts();

    Product getProduct(Product product);
}
