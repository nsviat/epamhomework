package User;

import Products.Product;

import java.util.ArrayList;

public class Cart implements CartService {

    private double summ = 0;

    public ArrayList<Product> getProducts() {
        return products;
    }

    private ArrayList<Product> products = new ArrayList<>();

    public double getSumm() {
        return summ;
    }

    @Override
    public void addProduct(Product product) {
        summ += product.getPrice();
        products.add(product);
    }

    @Override
    public void addProducts(ArrayList<Product> product) {
        for (Product p :
                product) {
            summ += p.getPrice();
        }
        this.products.addAll(product);
    }

    @Override
    public boolean removeProduct(Product product) {
        if ((getProduct(product)) != null) {
            summ -= product.getPrice();
            products.remove(product);
            return true;
        } else
            return false;
    }

    @Override
    public void removeAllProducts() {
        this.products.clear();
        this.summ = 0;
    }

    @Override
    public Product getProduct(Product product) {
        for (Product p :
                products) {
            if (p.getName().equals(product.getName()))
                return p;
        }
        return null;
    }
}
