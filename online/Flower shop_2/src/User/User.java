package User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class User {
    private int id;
    private String name;
    private String adress;
    private double balance;

    private Cart cart = new Cart();

    private static Map<Integer, User> allUsers = new HashMap<Integer, User>();
    private static int countId = 1;


    User(String name, double balance) {
        this.id = countId;
        this.name = name;
        this.balance = balance;
        allUsers.put(countId, this);
        countId++;
    }

    public User(String name, String adress, double balance) {
        this.name = name;
        this.adress = adress;
        this.balance = balance;
        allUsers.put(countId, this);
        countId++;
    }



    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public double getBalance() {
        return balance;
    }

    public Cart getCart() {
        return cart;
    }

    public static User getUser(String name){
        for (User u :
                getAllUsers()) {
            if (u.getName().toLowerCase().equals(name.toLowerCase()))
                return u;
        }
        return null;
    }

    @Override
    public String toString() {
        return "{User{ name = " + name +
                ", balance = " + balance +
                "}";
    }

    public static List<User> getAllUsers(){
        return new ArrayList<>(allUsers.values());
    }

    public static int getHowManyUsers(){
        return allUsers.size();
    }
}
