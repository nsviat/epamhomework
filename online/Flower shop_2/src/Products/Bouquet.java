package Products;

import java.util.ArrayList;

public class Bouquet extends Product implements BouquetService {
    private ArrayList<Flower> flowers = new ArrayList<>();

    private int flowersNum;

    private Product wrapper;
    private Product ribbon;

    Bouquet(String name, double price) {
        super(name, price);
    }

    public Bouquet(String name, ArrayList<Flower> flowers) {
        super(name);
        setBouquetPrice(flowers);
        this.flowers = flowers;
        flowersNum = this.flowers.size();
    }

    public Bouquet(String name, double price, Wrapper wrapper, Ribbon ribbon) {
        super(name, price);
        this.wrapper = wrapper;
        this.ribbon = ribbon;
    }

    public Product getWrapper() {
        return wrapper;
    }

    public void addWrapper(Product wrapper) {
        this.wrapper = wrapper;
        this.setPrice(this.getPrice() + this.wrapper.getPrice());
    }

    public Product getRibbon() {
        return ribbon;
    }

    public void addRibbon(Product ribbon) {
        this.ribbon = ribbon;
        this.setPrice(this.getPrice() + this.ribbon.getPrice());
    }

    public int getFlowersNumber() {
        return flowersNum;
    }

    private void setBouquetPrice(ArrayList<Flower> flowers){
        for (Flower f:
                flowers) {
            this.setPrice(this.getPrice()+f.getPrice());
        }
    }

    @Override
    public void addFlower(Flower flower) {
        flowers.add(flower);
        flowersNum++;
    }

    @Override
    public int findFlower(Flower flower) {
        int flowerNum = 0;
        for (Flower f : flowers) {
            if (f.getName().equals(flower.getName()))
                flowerNum++;
        }
        return flowerNum;
    }

    @Override
    public Flower getFlower(Flower flower) {
        for (Flower f : flowers) {
            if (f.getName().equals(flower.getName()))
                return f;
        }
        return null;
    }

    @Override
    public boolean removeFlower(Flower flower) {
        flowersNum--;
        return flowers.remove(flower);
    }

    @Override
    public String toString() {
        return "Bouquet{" +
                "name=" + getName() + '\'' +
                "price=" + getPrice() +
                '}';
    }
}
