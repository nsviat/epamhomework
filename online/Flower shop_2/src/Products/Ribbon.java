package Products;

public class Ribbon extends Product {

    private double length;
    private String color;

    public Ribbon(String name, double price) {
        super(name, price);
        this.length = 10;
        this.color = "green";
    }

    public Ribbon(String name, double price, double length, String color) {
        super(name, price);
        this.length = length;
        this.color = color;
    }

    public double getLength() {
        return length;
    }

    public String getColor() {
        return color;
    }
}