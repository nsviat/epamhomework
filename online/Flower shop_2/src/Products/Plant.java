package Products;

import Products.PruductsProperties.PropertiesContainer;
import Products.PruductsProperties.Property;

abstract class Plant extends Product {

    private String timeToLife;
    private String waterComsuming;
    protected Property property = PropertiesContainer.noneProperty;

    Plant(String name, double price) {
        super(name, price);
    }

    Plant(String name, double price, Property property) {
        super(name, price);
        this.property = property;
    }

    Plant(String name, double price, String description, String timeToLife,
          String waterComsuming, Property property) {
        super(name, price, description);
        this.property = property;
        this.timeToLife = timeToLife;
        this.waterComsuming = waterComsuming;
    }

    Plant(String name, double price, String description, String timeToLife, String waterComsuming) {
        super(name, price, description);
        this.timeToLife = timeToLife;
        this.waterComsuming = waterComsuming;
    }

    public Property getProperty() {
        return property;
    }

    public String getTimeToLife() {
        return timeToLife;
    }

    public void setTimeToLife(String timeToLife) {
        this.timeToLife = timeToLife;
    }

    public String getWaterComsuming() {
        return waterComsuming;
    }

    public void setWaterComsuming(String waterComsuming) {
        this.waterComsuming = waterComsuming;
    }
}
