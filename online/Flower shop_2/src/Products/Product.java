package Products;

public abstract class Product{
    private String name;
    private double price;
    private String description;

    Product(){};

    Product(String name, double price) {
        this.name = name;
        this.price = price;
    }

    Product(String name, double price, String description) {
        this.name = name;
        this.price = price;
        this.description = description;
    }

    Product(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public double changePrice() {
        return price;
    }

    void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
