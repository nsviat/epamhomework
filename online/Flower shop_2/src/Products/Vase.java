package Products;

class Vase extends Product {

    private String material;

    Vase(String name, double price) {
        super(name, price);
    }

    Vase(String name, double price, String material) {
        super(name, price);
        this.material = material;
    }
}
