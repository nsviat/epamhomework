package Products;

import Products.PruductsProperties.Property;

public class Flower extends Plant {
    private String color;

    public Flower(String name, double price) {
        super(name, price);
    }

    public Flower(String name, double price,Property property) {
        super(name, price, property);
    }

    public Flower(String name, double price, String description, String timeToLife, String waterComsuming, String color, Property property) {
        super(name, price, description, timeToLife, waterComsuming);
        this.property = property;
        this.color = color;
    }

    public Flower(String name, double price, String description, String timeToLife, String waterComsuming, String color) {
        super(name, price, description, timeToLife, waterComsuming);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        if (property.getPropertyName().equals("base") && getDescription() == null)
            return "Flower{" +
                    "name='" + this.getName() + '\'' +
                    ", price=" + this.getPrice() +
                    '}';
        else if (property.getPropertyName().equals("base"))
            return "Flower{" +
                    "name='" + this.getName() + '\'' +
                    ", price=" + this.getPrice() + '\'' +
                    ", description=" + this.getDescription() +
                    '}';
        else
            return "Flower{" +
                    "name='" + this.getName() + '\'' +
                    ", price=" + this.getPrice() + '\'' +
                    ", description=" + this.getDescription() + '\'' +
                    ", properties=" + this.getProperty().toString() +
                    '}';
    }
}
