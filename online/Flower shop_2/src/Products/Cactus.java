package Products;

import Products.PruductsProperties.PropertiesContainer;
import Products.PruductsProperties.Property;

public class Cactus extends Plant {

    private String color;

    public Cactus(String name, double price, String color) {
        super(name, price, PropertiesContainer.koluchka);
        this.color = color;
    }

    public Cactus(String name, double price, String color, Property property) {
        super(name, price, property);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "Cactus{" +
                "name='" + this.getName() + '\'' +
                ", price=" + this.getPrice() + '\'' +
                ", properties=" + this.getProperty().toString() +
                '}';
    }
}
