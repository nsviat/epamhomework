package Products;

import Products.PruductsProperties.PropertiesContainer;
import Products.PruductsProperties.Property;

public class Tree extends Plant {
    
    private boolean fruits_present;

    public Tree(String name, double price) {
        super(name, price);
    }

    public Tree(String name, double price, String description, String timeToLife, String waterConsuming, boolean fruits_present) {
        super(name, price, description, timeToLife, waterConsuming, PropertiesContainer.noneProperty);
        this.fruits_present = fruits_present;
    }

    public Tree(String name, double price, String description, String timeToLife, String waterConsuming, boolean fruits_present, Property property) {
        super(name, price, description, timeToLife, waterConsuming, property);
        this.fruits_present = fruits_present;
    }

    public boolean isFruits_present() {
        return fruits_present;
    }

    public void setFruits_present(boolean fruits_present) {
        this.fruits_present = fruits_present;
    }

    @Override
    public String toString() {
        return "Tree{" +
                "name='" + this.getName() + '\'' +
                ", price=" + this.getPrice() + '\'' +
                ", description=" + this.getDescription() + '\'' +
                ", properties=" + this.getProperty().toString() + '\'' +
                "fruits_present=" + fruits_present +
                '}';
    }
}
