package Products;

public class NoSuchObject extends Exception {
    public NoSuchObject(String message) {
        super(message);
    }
}
