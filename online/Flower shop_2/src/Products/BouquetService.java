package Products;

interface BouquetService {
    void addFlower(Flower flower);

    boolean removeFlower(Flower flower);

    int findFlower(Flower flower);

    Flower getFlower(Flower flower);
}
