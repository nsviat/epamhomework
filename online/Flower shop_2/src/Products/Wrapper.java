package Products;

public class Wrapper extends Product {
    private String transparency;  //прозорість
    private String picture;

    public Wrapper(String name, double price) {
        super(name, price);
        this.transparency = "full";
        this.picture = "flowers";
    }

    public Wrapper(String name, double price, String description, String transparency, String picture) {
        super(name, price, description);
        this.transparency = transparency;
        this.picture = picture;
    }

    public String getTransparency() {
        return transparency;
    }

    public String getPicture() {
        return picture;
    }
}
