package Products.PruductsProperties;

public class KoluchkaProperty extends Property {
    private int lengthKoluchki;
    private boolean toxic;

    public KoluchkaProperty(String propertyName, int lengthKoluchki, boolean toxic){
        super(propertyName);
        this.lengthKoluchki = lengthKoluchki;
        this.toxic = toxic;
    }

    public int getLengthKoluchki() {
        return lengthKoluchki;
    }

    public boolean isToxic() {
        return toxic;
    }

    @Override
    public boolean equalProperty(Property obj)
    {
        System.out.println(obj.getClass());
        if(obj == this)
            return true;

        if(obj == null)
            return false;

        if(getClass() != obj.getClass())
            return false;
        else
        {
            KoluchkaProperty tmp = (KoluchkaProperty) obj;
            if(tmp.lengthKoluchki == this.lengthKoluchki)
                return true;
            else
                return false;
        }
    }

    @Override
    public String toString() {
        return "KoluchkaProperty{" +
                "lengthKoluchki=" + lengthKoluchki +
                ", toxic=" + toxic +
                ", propertyName='" + propertyName + '\'' +
                '}';
    }
}
