package Products.PruductsProperties;

public class KushchProperty extends Property {

    private boolean berriesEnabled;
    private String branchDensity;

    public KushchProperty(String propertyName, boolean berriesEnabled, String branchDensity) {
        super(propertyName);
        this.berriesEnabled = berriesEnabled;
        this.branchDensity = branchDensity;
    }

    public boolean isBerriesEnabled() {
        return berriesEnabled;
    }

    public String getBranchDensity() {
        return branchDensity;
    }

    @Override
    public boolean equalProperty(Property obj) {
        System.out.println(obj.getClass());
        if(obj == this)
            return true;

        if(obj == null)
            return false;

        if(getClass() != obj.getClass())
            return false;
        else
        {
            KushchProperty tmp = (KushchProperty) obj;
            if(tmp.berriesEnabled == this.berriesEnabled && tmp.branchDensity.equals(this.branchDensity)) {
                return true;
            } else
                return false;
        }
    }

    @Override
    public String toString() {
        return "KushchProperty{" +
                "berriesEnabled=" + berriesEnabled +
                ", branchDensity='" + branchDensity + '\'' +
                ", propertyName='" + propertyName + '\'' +
                '}';
    }
}
