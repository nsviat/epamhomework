package Products.PruductsProperties;

public abstract class Property {

    protected String propertyName;



    protected Property(String propertyNmae){
        this.propertyName = propertyNmae;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public abstract boolean equalProperty(Property obj);

    @Override
    public String toString() {
        return "Property{" +
                "property name='" + propertyName + '\'' +
                '}';
    }
}
