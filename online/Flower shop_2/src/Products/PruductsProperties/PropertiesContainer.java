package Products.PruductsProperties;

import java.util.*;

public class PropertiesContainer {

    public static KoluchkaProperty koluchka = new KoluchkaProperty("baseKoluchka", 1, true);
    public static KushchProperty kushch = new KushchProperty("baseKushch", false, "default");
    public static Property noneProperty = new Property("base") {
        @Override
        public boolean equalProperty(Property obj) {
            return false;
        }
    };

    private static LinkedList<Property> properties = new LinkedList<>();

    public static void displayProperty(){
        properties.add(koluchka);
        properties.add(kushch);
        properties.add(noneProperty);

        for (Property p :
                properties) {
            System.out.println(p.toString());
        }
    }

    static void addProperty(Property p){
        properties.add(p);
    }

    public static LinkedList<Property> getProperties() {
        return properties;
    }

    public static Property getProperty(String name){
        for (Property p :
                properties)
            if (p.getPropertyName().equals(name))
                return p;
        return null;
    }
}
