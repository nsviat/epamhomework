package Products;

import Products.PruductsProperties.Property;

import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class ProductFactory {
    public static Plant createPlant(String plantType, String name, double price) throws NoSuchObject {
        Scanner sc = new Scanner(System.in);
        boolean fruits;
        switch (plantType){
            case "flower":
                return new Flower(name, price);
            case "cactus":
                return new Cactus(name, price,"Green");
            default:
                throw new NoSuchObject("There is no " + plantType + "plant");
        }
    }

    //For flowers with properties
    public static Flower createPlant(String plantType, String name, double price, String description, String timeToLife,
                                    String waterComsuming, String color, Property property) throws NoSuchObject {
        switch (plantType){
            case "flower":
                return new Flower(name, price, description, timeToLife, waterComsuming, color, property);
            default:
                    throw new NoSuchElementException("You entered incorrect name of product.");
        }
    }


    //just for flowers
    public static Flower createPlant(String plantType, String name, double price, String description, String timeToLife,
                                     String waterComsuming, String color) throws NoSuchObject {
        switch (plantType){
            case "flower":
                return new Flower(name, price, description, timeToLife, waterComsuming, color);
            default:
                throw new NoSuchElementException("You entered incorrect name of product.");
        }
    }

    //for cactus with property
    public static Cactus createPlant(String plantType, String name, double price, String color, Property property) throws NoSuchObject {
        switch (plantType) {
            case "cactus":
                return new Cactus(name, price, color, property);
            default:
                throw new NoSuchElementException("You entered incorrect name of product.");
        }
    }

    //just for cactus
    public static Cactus createPlant(String plantType, String name, double price, String color) throws NoSuchObject {
        switch (plantType) {
            case "cactus":
                return new Cactus(name, price, color);
            default:
                throw new NoSuchElementException("You entered incorrect name of product.");
        }
    }

    //just for tree
    public static Tree createPlant(String plantType, String name, double price, String description, String timeToLife,
                                     String waterComsuming, boolean fruitsEnabled) throws NoSuchObject {
        switch (plantType){
            case "tree":
                return new Tree(name, price, description, timeToLife, waterComsuming, fruitsEnabled);
            default:
                throw new NoSuchElementException("You entered incorrect name of product.");
        }
    }

    //for tree  with properties
    public static Tree createPlant(String plantType, String name, double price, String description, String timeToLife,
                                   String waterComsuming, boolean fruitsEnabled, Property property) throws NoSuchObject {
        switch (plantType){
            case "tree":
                return new Tree(name, price, description, timeToLife, waterComsuming, fruitsEnabled, property);
            default:
                throw new NoSuchElementException("You entered incorrect name of product.");
        }
    }

    public static Product createProduct(String productType, String name, double price) throws NoSuchObject {
        Scanner sc = new Scanner(System.in);
        switch (productType){
            case "vase":
                String material = sc.next();
                return new Vase(name, price, material);
            case "bouquet":
                return new Bouquet(name, price);
            case "wrapper":
                return new Wrapper(name, price);
            case "ribbon":
                return new Ribbon(name, price);
                default:
                    throw new NoSuchObject("There is no " + productType + "product");
        }
    }

    public static Vase createProduct(String productType, String name, double price, String material) throws NoSuchObject {
        Scanner sc = new Scanner(System.in);
        switch (productType){
            case "vase":
                return new Vase(name,price, material);
            default:
                throw new NoSuchObject("There is no " + productType + "product");
        }
    }

    public static Bouquet createProduct(String productType, String name, ArrayList<Flower> flowers) throws NoSuchObject {
        if (productType.equals("bouquet"))
            return new Bouquet(name, flowers);
        else
            throw new NoSuchObject("There is no " + productType + "product");
    }
}
