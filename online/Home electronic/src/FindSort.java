import java.util.Comparator;
import java.util.LinkedList;

public interface FindSort {



    //sort by energy costs
    static LinkedList<Device> sortByEnergy(LinkedList<Device> devices) {
        LinkedList<Device> fz = devices;

        fz.sort(new Comparator<Device>() {
            public int compare(Device d1, Device d2) {
                return d1.getEnergyPerHour().compareTo(d2.getEnergyPerHour());
            }
        });
        return fz;
    }

    //find device by name and energy consuming
    static LinkedList<Device> findDevice(LinkedList<Device> devices, String name,
                                         double energyPerHour, int puhybka) {
        for (Device d : devices) {
            if (!d.get_name().equals(name) || (d.getEnergyPerHour() + puhybka > energyPerHour ||
                    d.getEnergyPerHour() - puhybka < energyPerHour))
                devices.remove(d);
        }
        return devices;
    }
}
