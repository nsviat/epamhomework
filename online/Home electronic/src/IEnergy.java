import java.util.LinkedList;
import java.util.List;

public interface IEnergy {
    Double getEnergyPerHour();


    //getting amount of user energy
    double getUsedEnergy();

    //count energy which device consume per hour
    static double countAllEnergy(LinkedList<Device> devices) {
        double allEnergyPerHour = 0;

        for (Device d : devices)
            allEnergyPerHour += d.getEnergyPerHour();

        return allEnergyPerHour;
    }
}
