import java.util.Comparator;
import java.util.LinkedList;

abstract class Device extends ObjectLive implements IEnergy,FindSort{
    protected Double energyPerHour;
    private String name;

    Device(String name, Double energyPerHour, boolean turned) {
        super(turned);
        this.name = name;
        this.energyPerHour = energyPerHour;
        super.turned = turned;
    }

    String get_name() {
        return name;
    }
}
