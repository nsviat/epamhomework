import java.util.LinkedList;
//стан фасад
public class Main {
    public static void main(String[] args) throws Exception {
        Freezer f1 = new Freezer("freezer1", 13.23, false, -30);
        f1.on();
        Thread.sleep(3000);
        f1.off();
        Thread.sleep(1000);
        Freezer f2 = new Freezer("freezer2", 16.23, true, -26);
        System.out.println("f2 time:" + f2.timeAlive);
        Thread.sleep(2000);
        f2.on();
        Thread.sleep(3000);
        f2.off();

        LinkedList<Device> devices = new LinkedList<>();
        devices.add(f1);
        devices.add(f2);

        FindSort.sortByEnergy(devices);
    }
}
