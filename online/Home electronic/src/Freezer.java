import java.util.LinkedList;

public class Freezer<E>  extends Device {
    private double minTemp;

    //products here
    LinkedList<E> products = new LinkedList<E>();

    Freezer(String name, Double energyPerHour, boolean turned, double minTemp){
        super(name, energyPerHour, turned);
        this.minTemp = minTemp;
    }

    public double getUsedEnergy() {
        return getEnergyPerHour() * timeAlive;
    }

    public Double getEnergyPerHour() {
        return energyPerHour;
    }

    public double getMinTemp() { return minTemp; }



    //some functions
}
