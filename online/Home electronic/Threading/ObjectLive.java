import java.util.Date;

abstract class ObjectLive extends Thread implements TimeExecution {
    int timeAlive = 0;
    boolean turned=false;

    ObjectLive(boolean turned) {
        this.turned = turned;
        if (this.turned)
            start();
    }

    //start counter
    public void on() {
        if (!this.turned) {
            this.turned = true;
            start();
        }
    }

    //turn off counter
    public void off() {
        this.turned = false;
    }

    //thread
    public void run() {
        //deretmine seconds
        final Date createdDate = new java.util.Date();
        while (this.turned) {
            java.util.Date now = new java.util.Date();
            timeAlive = (int) ((now.getTime() - createdDate.getTime()) / 1000) + 1;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(timeAlive);
        }
    }
}
