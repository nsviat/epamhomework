public interface TimeExecution {

    //start counter
    void on();

    //turning off counter
    void off();
}
